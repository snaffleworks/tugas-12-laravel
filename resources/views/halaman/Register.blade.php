<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h1>Buat Akun Baru!</h1>
        <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
    
        <label>First Name:</label><br><br>
        <input type="text" name="depan"><br><br>
        
        <label>Last Name:</label><br><br>
        <input type="text" name="belakang"> <br><br>
        
        <label>Gender</label> <br><br>
        <input type="radio" name="gender" value="M"> Male <br>
        <input type="radio" name="gender" value="F"> Female <br>
        <input type="radio" name="gender" value="O"> Other
            
        <label>Nationality</label><br><br>
            <select>
                <option>Indonesian</option>
                <option>America</option>
                <option>Inggris</option>
            </select>
          
        <label>Language Spoken</label><br><br>
        <input type="checkbox" name="Bahasa Indonesia" value="Bahasa Indonesia" id="Bahasa Indonesia"> <label for="Bahasa Indonesia">Bahasa Indonesia</label><br>
        <input type="checkbox" name="English" value="English" id="English"> <label for="English">English</label><br>
        <input type="checkbox" name="Other" value="Other" id="Other"> <label for="Other">Other</label>
            
        <label> Bio:</label><br><br>
        <textarea name="Bio:" id="" cols="40" rows="10"></textarea><br>
            
        <input type="submit" value="Sign Up">
    </form>

</body>
</html>