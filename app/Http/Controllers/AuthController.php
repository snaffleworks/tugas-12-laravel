<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
   public function Register(){
      return view('halaman.Register');
   }

   public function SignUp(Request $request){
       $depan = $request['First Name'];
       $belakang = $request['Last Name'];
       return view('halaman.Welcome', compact('depan','belakang'));
   }
}